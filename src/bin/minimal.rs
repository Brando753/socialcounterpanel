#![no_main]
#![no_std]
#![feature(type_alias_impl_trait)]

use social_counter as _; // global logger + panicking-behavior + memory layout

#[rtic::app(
    device = stm32l0xx_hal::pac,
    dispatchers = [SPI1, USART1, RTC, WWDG]
)]
mod app {
    // Shared resources go here
    #[shared]
    struct Shared {
        // TODO: Add resources
    }

    // Local resources go here
    #[local]
    struct Local {
        // TODO: Add resources
    }

    #[init]
    fn init(cx: init::Context) -> (Shared, Local) {
        defmt::info!("init");

        // TODO setup monotonic if used
        let sysclk = { /* clock setup + returning sysclk as an u32 */ };
        let token = rtic_monotonics::create_systick_token!();
        rtic_monotonics::systick::Systick::new(cx.core.SYST, sysclk, token);


        // task1::spawn().ok();

        (
            Shared {
                // Initialization of shared resources go here
            },
            Local {
                // Initialization of local resources go here
            },
        )
    }

    // Optional idle, can be removed if not needed.
    #[idle]
    fn idle(_: idle::Context) -> ! {
        defmt::info!("idle");

        loop {
            continue;
        }
    }

    // // TODO: Add tasks
    // #[task]
    // async fn task1(_cx: task1::Context) {
    //     defmt::info!("Hello from task1!");
    // }
}
